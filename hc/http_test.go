package hc

import (
	"testing"

	"gitlab.com/my0sot1s/helper"
)

func TestGetRaw(t *testing.T) {
	bin, _ := GetRaw("http://coraline-x.herokuapp.com/", nil)
	helper.Log(string(bin))
}
func TestGetJSON(t *testing.T) {
	bin, _ := GetJSON("https://reqres.in/api/users?page=2", nil)
	helper.Log(bin)
}
func TestPostRaw(t *testing.T) {
	data := make(map[string]interface{})
	data["name"] = "morpheus"
	data["job"] = "leader"
	bin, _ := PostRaw("https://reqres.in/api/users2", data)
	helper.Log(string(bin))
}

func TestPostJSON(t *testing.T) {
	data := make(map[string]interface{})
	data["name"] = "morpheus"
	data["job"] = "leader"
	bin, _ := PostJSON("https://reqres.in/api/users2", data)
	helper.Log(bin)
}
