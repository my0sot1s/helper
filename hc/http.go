package hc

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gitlab.com/my0sot1s/helper"
)

func get(url string, options map[string]string) ([]byte, error) {
	resp, err := http.Get(url)
	defer resp.Body.Close()
	if err != nil {
		return nil, err
	}
	return ioutil.ReadAll(resp.Body)
}

// GetRaw get bin
func GetRaw(url string, options helper.MS) ([]byte, error) {
	return get(url, options)
}

// GetJSON get map
func GetJSON(url string, options helper.MS) (helper.M, error) {
	bin, err := get(url, options)
	if err != nil {
		return nil, err
	}
	obj := make(helper.M)
	err = json.Unmarshal(bin, &obj)
	if err != nil {
		return nil, err
	}
	return obj, nil
}

func post(url string, options helper.MS, payload helper.M) ([]byte, error) {
	bin, err := json.Marshal(payload)
	if err != nil {
		return nil, err
	}
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(bin))
	req.Header.Set("Content-Type", "application/json")
	for k, v := range options {
		req.Header.Set(k, v)
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	return ioutil.ReadAll(resp.Body)
}

// PostRaw get raw value
func PostRaw(url string, options helper.MS, payload helper.M) ([]byte, error) {
	return post(url, options, payload)
}

// PostJSON support parse to map
func PostJSON(url string, options helper.MS, payload helper.M) (helper.M, error) {
	resp, err := post(url, options, payload)
	if err != nil {
		return nil, err
	}
	obj := make(helper.M)
	err = json.Unmarshal(resp, &obj)
	if err != nil {
		return nil, err
	}
	return obj, nil
}
